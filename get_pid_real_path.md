# Linux中获取进程其运行程序的路径

下面会打印出：

PID：进程号

socket：端口号

prodir： 进程其运行程序的绝对路径

```
#!/bin/bash

#Pid=`netstat -tlnp|grep java|awk '{print $7}'|awk -F '/' '{print $1}'`
Pid=`ps aux|grep "/opt"|grep -v grep |awk '{print $2}'`

for i in  $Pid;do
  pdir=`ps axu|grep $i`
  prodir=`ls -l /proc/$i/cwd`
  psocket=`netstat -tlnp|grep $i|awk '{print $4}'|awk -F ':' '{print $2}'`
  echo "PID:"$i
  echo "Socket:"$psocket
  echo "-----"
  echo "Program dir:"
  #echo $pdir
  echo $prodir
  echo "----------------------"
done
```

