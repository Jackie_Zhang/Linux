# Linux command

## ls命令

### 列出inode

```
ls -li
```

## find命令

### 删除某inode文件或目录

可用于删除特殊文件、目录

inum后面跟的是某inode的值

```
find ./ -inum 209919694 -print -exec rm {} -rf \;
```
