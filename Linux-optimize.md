# Linux 优化

## 网络优化

vim /etc/sysctl.conf

修改完下面配置执行 **sysctl -p** 立即生效，重启终端可以查看效果

```
# Enable SYN cookies to defeat SYN flood attacks
net.ipv4.tcp_syncookies = 1

# Increases the backlog queue (default value[1024])
net.ipv4.tcp_max_syn_backlog = 4096

# Decrease the time default value[60] for tcp_fin_timeout connection
net.ipv4.tcp_fin_timeout = 15

# Decrease the time default value[7200] for tcp_keepalive_time connection
net.ipv4.tcp_keepalive_time = 1800

# Enable ignoring broadcasts request
net.ipv4.icmp_echo_ignore_broadcasts = 1

# Enable bad error message Protection
net.ipv4.icmp_ignore_bogus_error_responses = 1

net.ipv4.tcp_retrans_collapse = 0
vm.swappiness = 0

kernel.pid_max = 32768
fs.file-max = 1250000
```

## ulimit

修改完下面配置执行sysctl -p立即生效，重启终端可以查看效果

**对于运行的程序需要重启服务**

对当前用户生效：

ulimit -HSn 655350

永久生效：

vim /etc/security/limits.conf

添加以下内容：

```
* soft nproc 386820
* hard nproc 386820

* soft nofile 655350
* hard nofile 655350
```

**centos 6**

vim /etc/security/limits.d/90-nproc.conf

修改以下内容：

```
*      soft  nproc 386820
root   soft  nproc unlimited
```

**centos7**

*注意：以下针对PAM登录有效，对system services无效，需要修改/usr/lib/systemd/system/服务名.service中LimitNOFILE值，然后systemctl daemon-reload && systemctl restart 服务名.service*

vim  /etc/security/limits.d/20-nproc.conf 

修改以下内容：

```
*      soft  nproc 386820
root   soft  nproc unlimited
```
为实现对普通用户限制max user processes

对system services服务限制：

需要修改：/etc/systemd/system.conf

```
DefaultLimitCORE=infinity
DefaultLimitNOFILE=386820
DefaultLimitNPROC=386820
```

只有重启系统或者执行`systemctl daemon-reexec`才可以生效


## CPU优化

Centos 6、7都适用
默认是10

```
sysctl -w kernel.watchdog_thresh=30
echo 30 >  /proc/sys/kernel/watchdog_thresh
```

## 关闭SElinux

```    
setenforce 0
sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config
```


## 防火墙

### CentOS 7

关闭firewall：

```
systemctl stop firewalld.service #停止firewall
systemctl disable firewalld.service #禁止firewall开机启动
```

若iptables未安装，安装：


```
yum install iptables-services 
```

### CentOS 5/6

/etc/sysconfig/iptables

iptables -F

vim /etc/sysconfig/iptables

```
*filter
:INPUT ACCEPT [844773:76344355]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [98086:8772090]
-A INPUT -p tcp -m tcp --dport 80 -j ACCEPT
-A INPUT -s 127.0.0.1 -p tcp -j ACCEPT
-A INPUT -s 10.0.0.0/255.0.0.0 -p tcp -j ACCEPT
-A INPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,ACK SYN -j DROP

COMMIT 
```

/etc/init.d/iptables restart


一个实例脚本：

并加入到/etc/rc.local中

sh /etc/rc.d/rc.fw

rc.fw内容如下：

```
#!/bin/sh


IPTABLES=/sbin/iptables
IDC_TABLE=/etc/mops/idc_table.list
IDC_TABLE_D=/etc/mops/fw.d


# clear
$IPTABLES -F

test -d $IDC_TABLE_D || mkdir -p $IDC_TABLE_D 

# if pkg type is allow, then accept
#$IPTABLES -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

# if pkg visit 80,7710 port then accept
$IPTABLES -A INPUT -p tcp --dport 80 -j ACCEPT

# if pkg from allow ip then accept
$IPTABLES -A INPUT -p tcp -s 127.0.0.1  -j ACCEPT
$IPTABLES -A INPUT -p tcp -s 10.0.0.0/8  -j ACCEPT


for f in ${IDC_TABLE} ${IDC_TABLE_D%/}/*.list
do
	if test -f $f; then
		grep -Phv '^\s*#|^\s*$' $f |
		while read sub
		do
			len=`echo $sub|wc -m`
			if test "x$len" != "x" && test $len -lt 20 ;then
				$IPTABLES -A INPUT -p tcp -s $sub  -j ACCEPT
			fi
		done
	fi
done

# if pkg not above then deny
$IPTABLES -A INPUT -p tcp --syn -j DROP
```


/etc/mops/idc_table.list内容例子：

```
    220.181.88.8/30
    220.181.92.0/24
    58.83.132.0/25
```


## ssh

禁止密码登陆

```
cp /etc/ssh/sshd_config /etc/ssh/sshd_config.$(date +%F)
#sed -i 's/#Port 22/Port 52113/g' /etc/ssh/sshd_config
sed -i 's/#PermitEmptyPasswords no/PermitEmptyPasswords no/g' /etc/ssh/sshd_config
sed -i 's/#PermitRootLogin yes/PermitRootLogin no/g' /etc/ssh/sshd_config
sed -i 's/#UseDNS yes/UseDNS no/g' /etc/ssh/sshd_config
```
/etc/init.d/sshd reload


## NTP

每台服务器统一NTP，使用自己私有NTP或者公共NTP服务器。

**公共NTP服务器：**

```
ntpdate cn.pool.ntp.org
```

**私有NTP服务：**


实例：

NTP服务器：10.8.64.130


服务器端：

vim /etc/ntp.conf

```
restrict default kod nomodify notrap nopeer noquery
restrict -6 default kod nomodify notrap nopeer noquery

restrict 127.0.0.1
restrict 10.8.64.0 mask 255.255.255.0 nomodify


server 193.228.143.24
server 51.174.131.248
server 212.47.249.141

server  127.127.1.0
fudge   127.127.1.0 stratum 10

driftfile /var/lib/ntp/drift

keys /etc/ntp/keys
```

/etc/init.d/ntpd restart


客户端：

vim /etc/ntp.conf

```
restrict default kod nomodify notrap nopeer noquery
restrict -6 default kod nomodify notrap nopeer noquery
restrict 127.0.0.1

server 10.8.64.130

server  127.127.1.0
fudge   127.127.1.0 stratum 10
driftfile /var/lib/ntp/drift
keys /etc/ntp/keys
```

/etc/init.d/ntpd restart