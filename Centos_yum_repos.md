# CentOS的三大yum源

## 1. EPEL（企业版 Linux 附加软件包）
 官方网站：https://fedoraproject.org/wiki/EPEL/zh-cn
 当 Fedora 项目的维护人员发现可以采用管理 Fedora 项目的方法，来管理针对企业版 Linux 的附加软件包项目时，一个新的伟大的项目诞生了！项目诞生之初只是加入了一些在 RHEL 维护 Fedora 的工具。随着时间的发展，EPEL 仓库越来越丰富，成为一个大型的软件收集仓库。安装epel源的好处就是epel这个项目是由fedora维护的，在维护的这个源中包含许多新的软件。
 
安装
```
yum install epel-release

```

参考网址：

http://www.centoscn.com/yunwei/news/2015/1231/6596.html


**阿里云centos数据源：**

http://mirrors.aliyun.com/repo/epel-7.repo

http://mirrors.aliyun.com/repo/epel-6.repo

http://mirrors.aliyun.com/repo/Centos-5.repo

上面的可以在下面查询到：

http://mirrors.aliyun.com/repo/

安装：

```
wget -O /etc/yum.repos.d/aliyun.repo http://mirrors.aliyun.com/repo/epel-6.repo
```

同时支持https

## 2 .RPMForge
 
RPMForge是CentOS系统下的软件仓库，拥有4000多种的软件包，被CentOS社区认为是最安全也是最稳定的一个软件仓库。
RPMForge官方网站：http://rpmforge.net/
安装 

```
EL 7: x86_64
EL 6: i686, x86_64
EL 5: i386, x86_64, ppc
EL 4: i386, x86_64, ppc
EL 3: i386, x86_64
```

## 3.elrepo
 
ELRepo项目侧重于硬件相关的包来增强你的经验与企业Linux。这包括文件系统驱动,显卡驱动,网络驱动程序,声音驱动,摄像头和视频驱动程序。
 
Import the public key:

```
rpm --import https://www.elrepo.org/RPM-GPG-KEY-elrepo.org
```

To install ELRepo for RHEL-7, SL-7 or CentOS-7:

```
rpm -Uvh http://www.elrepo.org/elrepo-release-7.0-2.el7.elrepo.noarch.rpm
```

To install ELRepo for RHEL-6, SL-6 or CentOS-6:

```
rpm -Uvh http://www.elrepo.org/elrepo-release-6-6.el6.elrepo.noarch.rpm
```

To install ELRepo for RHEL-5, SL-5 or CentOS-5:

```
rpm -Uvh http://www.elrepo.org/elrepo-release-5-5.el5.elrepo.noarch.rpm
```

## 参考网址

官网数据源：

https://dl.fedoraproject.org/pub/epel/6/x86_64/

导入key：

https://getfedora.org/keys/faq/


其他数据源：

https://mirrors.tuna.tsinghua.edu.cn/epel/6/x86_64/

rpm包下载地址：

http://rpmfind.net/linux/rpm2html/search.php?query=perl-libs&submit=Search+...&system=&arch=

